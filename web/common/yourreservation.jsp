<%-- 
    Document   : vaccinepage
    Created on : Feb 15, 2022, 9:58:04 AM
    Author     : haipr
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %> 
<!DOCTYPE html>
<html>
    <!-- ======= header ======= -->
    <%@include file="../components/header.jsp" %>
    <main id="main">

        <!-- ======= Breadcrumbs Section ======= -->
        <section class="breadcrumbs">
            <div class="container">

                <div class="d-flex justify-content-between align-items-center">
                    <h2>Inner Page</h2>
                    <ol>
                        <li><a href="index.html">Home</a></li>
                        <li>Inner Page</li>
                    </ol>
                </div>

            </div>
        </section><!-- End Breadcrumbs Section -->

        <%ResultSet rsgetresevation = (ResultSet) request.getAttribute("rsgetresevation");%>
        <%String customername = (String) request.getAttribute("customername");%>


        <div class="container">
            <div class="row">
                <div class="d-flex justify-content-center" style="margin-top: 2%;color: black">
                    <h3>reservation of <%= customername%></h3>
                </div>
            </div>

            <div class="col-md-12">
                <table class="table table-striped" border="0" >
                    <thead>
                        <tr>
                            <th>Vaccine Image</th>
                            <th>Vaccine Name</th>
                            <th>Booking Date</th>
                            <th>Slot Begin</th>
                            <th>Slot End</th>
                            <th>Doctor Name</th>
                            <th>Status</th>
                            <th>Doctor Note</th>
                            <th>Your Health After Injection</th>
                            <th>Update Health Infor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%   while (rsgetresevation.next()) {%>
                        <tr>
                            <td><a href="detail?VaccineId=<%= rsgetresevation.getString(9)%>"><img src="common/homeAssets/img/<%= rsgetresevation.getString(1)%>" style="height: 100px ; width: 100px"></a></td>
                            <td><a href="detail?VaccineId=<%= rsgetresevation.getString(9)%>" style="color: #1977cc"><%= rsgetresevation.getString(2)%></a></td>
                            <td><%= rsgetresevation.getDate(3)%></td>
                            <td><%= rsgetresevation.getString(7)%></td>
                            <td><%= rsgetresevation.getString(8)%></td>
                            <td style="color: #1977cc"><a href="profiledrhomecontroller?DoctorID=<%= rsgetresevation.getInt(13)%>"><%= rsgetresevation.getString(5)%></a></td>
                            <td><%= rsgetresevation.getString(6)%></td>
                            <td><%= rsgetresevation.getString(12)%></td>
                            <td><%= rsgetresevation.getString(15)%></td>
                            <td><a href="updatehealthform?VaccineId=<%= rsgetresevation.getString(9)%>&CusId=<%= rsgetresevation.getString(10)%>&ResId=<%= rsgetresevation.getString(14)%>">Update</a></td>
                        </tr>
                        <%}%>
                    </tbody>
                </table>
            </div>
        </div>

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <%@include file="../components/footer.jsp" %>

    <!--<div id="preloader"></div>-->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="homeAssets/vendor/purecounter/purecounter.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/glightbox/js/glightbox.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/swiper/swiper-bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/php-email-form/validate.js" type="text/javascript"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>
</html>
