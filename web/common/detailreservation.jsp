<%-- 
    Document   : vaccinepage
    Created on : Feb 15, 2022, 9:58:04 AM
    Author     : haipr
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %> 
<!DOCTYPE html>
<html>
    <!-- ======= header ======= -->
    <%@include file="../components/header.jsp" %>
    <main id="main">

        <!-- ======= Breadcrumbs Section ======= -->
        <section class="breadcrumbs">
            <div class="container">

                <div class="d-flex justify-content-between align-items-center">
                    <h2>Inner Page</h2>
                    <ol>
                        <li><a href="index.html">Home</a></li>
                        <li>Inner Page</li>
                    </ol>
                </div>

            </div>
        </section><!-- End Breadcrumbs Section -->
        <div class="container">
            <div class="row">
                <div class="container-fluid"> 
                    <div class="page-wrapper">
                        <div class="content"style="justify-content: center">
                            <form method="POST" action="updatehealthform">

                                <div class="container" style="margin-top: 5rem">
                                    <% ResultSet rsgetvaccinerecord = (ResultSet) request.getAttribute("rsgetvaccinerecord");
                                        while (rsgetvaccinerecord.next()) {
                                    %>
                                    <div class="row mt-5">
                                        <div class="col-md-6 pl-5">
                                            <input type="hidden" name="vacid" value="<%= rsgetvaccinerecord.getInt(9)%>" />
                                            <input type="hidden" name="cusid" value="<%= rsgetvaccinerecord.getInt(10)%>" />
                                            <input type="hidden" name="cusname" value="<%= rsgetvaccinerecord.getString(11)%>" />
                                            <input type="hidden" name="resid" value="<%= rsgetvaccinerecord.getString(13)%>" />

                                            <h6 class="mt-4"><b>Customer Name : <%= rsgetvaccinerecord.getString(11)%></b></h6>
                                            <hr>
                                            <h6 class="mt-4"><b>Vaccine Image : <img src="common/homeAssets/img/<%= rsgetvaccinerecord.getString(1)%>" style="height: 100px ; width: 100px"></b></h6>
                                            <hr>
                                            <h6 class="mt-4"><b>Vaccine Name : <%= rsgetvaccinerecord.getString(2)%></b></h6>
                                            <hr>
                                            <h6 class="mt-4"><b>Injection Date : <%= rsgetvaccinerecord.getDate(3)%></b></h6>
                                            <hr>
                                            <h6 class="mt-4"><b>Begin Time : <%= rsgetvaccinerecord.getString(7)%></b></h6>
                                            <hr>
                                            <h6 class="mt-4"><b>End Time : <%= rsgetvaccinerecord.getString(8)%></b></h6>
                                            <hr>
                                            <p class="mt-4"><b>Health Condition <input type="text" name="cusleavehealthinfor" /></p>
                                            <hr>
                                            <button type="submit" name="submit" class="btn btn-primary submit-btn">Submit</button>

                                        </div>
                                    </div>
                                    <%}%>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <%@include file="../components/footer.jsp" %>

    <!--<div id="preloader"></div>-->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="homeAssets/vendor/purecounter/purecounter.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/glightbox/js/glightbox.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/swiper/swiper-bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/php-email-form/validate.js" type="text/javascript"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>
</html>
