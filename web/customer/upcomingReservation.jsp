<%-- 
    Document   : upcomingReservation
    Created on : Mar 24, 2022, 9:16:15 PM
    Author     : hoang
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %> 
<!DOCTYPE html>
<html>
    <!-- ======= header ======= -->
    <%@include file="../components/header.jsp" %>
    <main id="main">

        <!-- ======= Breadcrumbs Section ======= -->
        <section class="breadcrumbs">
            <div class="container">
            </div>
        </section><!-- End Breadcrumbs Section -->

        <c:choose>
            <c:when test="${empty requestScope.reservationList}">
                <div class="container">
                    <div class="d-flex justify-content-center">
                        <i class="fa-solid fa-calendar-circle-user" style="color: blue"></i>
                    </div>
                    <div class="d-flex justify-content-center">
                        <div class="card text-center">
                            <div class="card-header">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Attention</h5>
                                <p class="card-text">You have no upcoming injection yet, may be your reservation is waiting to be processed </p>
                                <a href="home" class="btn btn-primary">Home</a>
                            </div>
                            <div class="card-footer text-muted">
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="container">
                    <div class="row">
                        <div class="d-flex justify-content-center" style="margin-top: 2%">
                            <h3>Upcoming reservation of ${requestScope.customerName}</h3>
                        </div>
                    </div>
                    <table class="table table-striped" border="0" >
                        <thead>
                            <tr>
                                <th>Vaccine Image</th>
                                <th>Vaccine Name</th>
                                <th>Booking Date</th>
                                <th>Slot Begin</th>
                                <th>Slot End</th>
                                <th>Doctor Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.reservationList}" var="r">
                                <tr>
                                    <td><a href="detail?VaccineId=${r.vaccineId}"><img src="common/homeAssets/img/${r.vaccineImage}" style="height: 100px ; width: 100px"></a></td>
                                    <td><a href="detail?VaccineId=${r.vaccineId}">${r.vaccineName}</td>
                                    <td>${r.bookingDate}</td>
                                    <td>${r.slotBegin}</td>
                                    <td>${r.slotEnd}</td>
                                    <td><a href="profiledrhomecontroller?DoctorID=${r.doctorId}">${r.doctorName}</a></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>

            </c:otherwise>
        </c:choose>





    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <%@include file="../components/footer.jsp" %>

    <!--<div id="preloader"></div>-->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="homeAssets/vendor/purecounter/purecounter.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/glightbox/js/glightbox.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/swiper/swiper-bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/php-email-form/validate.js" type="text/javascript"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>
</html>
