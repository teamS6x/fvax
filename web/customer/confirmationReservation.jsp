<%-- 
    Document   : confirmationReservation
    Created on : Mar 24, 2022, 9:17:57 PM
    Author     : hoang
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %> 
<!DOCTYPE html>
<html>
    <!-- ======= header ======= -->
    <%@include file="../components/header.jsp" %>
    <main id="main">

        <!-- ======= Breadcrumbs Section ======= -->
        <section class="breadcrumbs">
            <div class="container">

                <div class="d-flex justify-content-between align-items-center">
                    <h2>Inner Page</h2>
                    <ol>
                        <li><a href="index.html">Home</a></li>
                        <li>Inner Page</li>
                    </ol>
                </div>
            </div>
        </section><!-- End Breadcrumbs Section -->



        <c:choose>
            <c:when test="${requestScope.status == 'failed'}">
                <div class="d-flex justify-content-center">
                    <i class="fa fa-times-circle fa-10x" style="color:red"></i>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="card text-center">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Warning</h5>
                            <p class="card-text">${requestScope.anounce}</p>
                            <a href="injection-booking" class="btn btn-primary">Back</a>
                            <a href="home" class="btn btn-primary">Home</a>
                        </div>
                        <div class="card-footer text-muted">
                        </div>
                    </div>
                </div>
            </c:when>    
            <c:otherwise>
                <div class="d-flex justify-content-center">
                    <i class="fa fa-check-circle fa-10x" style="color:green"></i>
                </div>
                <div class="d-flex justify-content-center">
                    <div class="card text-center">
                        <div class="card-header">
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">Reservation Reserved</h5>
                            <p class="card-text">You have made a reservation for your injection at</p>
                            <p class="card-text"><b>${requestScope.date}</b></p>
                            <p class="card-text">You have 1 unpaid bill do you want to pay now</p>
                            <a href="showbill" class="btn btn-primary">Yes I do</a>
                            <a href="home" class="btn btn-danger">Later</a>
                        </div>
                        <div class="card-footer text-muted">
                        </div>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>




    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <%@include file="../components/footer.jsp" %>

    <!--<div id="preloader"></div>-->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="homeAssets/vendor/purecounter/purecounter.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/glightbox/js/glightbox.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/swiper/swiper-bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/php-email-form/validate.js" type="text/javascript"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>
</html>
