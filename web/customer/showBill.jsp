<%-- 
    Document   : showBill
    Created on : Mar 24, 2022, 9:37:39 PM
    Author     : dattrinh
--%>

<%-- 
    Document   : vaccinepage
    Created on : Feb 15, 2022, 9:58:04 AM
    Author     : haipr
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %> 

<!DOCTYPE html>
<html>
    <!-- ======= header ======= -->
    <%@include file="../components/header.jsp" %>
    <main id="main">

        <!-- ======= Breadcrumbs Section ======= -->
        <section class="breadcrumbs">
            <div class="container" style="justify-content: center">

                <div class="d-flex justify-content-between align-items-center">
                    <h2>Inner Page</h2>
                    <ol>
                        <li><a href="index.html">Home</a></li>
                        <li>Inner Page</li>
                    </ol>
                </div>

            </div>
        </section><!-- End Breadcrumbs Section -->
        <div class="container">
            <div class="col-md-12">
                <table class="table table-striped" border="0">
                    <thead>
                        <tr>
                            <th>Price</th>
                            <th>Vaccine</th>
                            <th>Package</th>
                            <th>Created Date</th>
                            <th>Status</th>
                            <th>Payment</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${requestScope.list}" var="a">
                            <tr>
                             <td style="font-weight: bold;font-style: italic;color: #1435C3">
                                <fmt:formatNumber type="number" groupingUsed="true" value="${a.total_Price}"  /> <span>VND</span>
                            </td>
                                <td>${a.vaccineName}</td>
                                <td>${a.packageName}</td>
                                <td>${a.date}</td>
                                <c:if test="${a.status eq false}">
                                    <td> Not pay yet</td>
                                    <td>
                                        <form action="payment" method="post">
                                            <input type="hidden" name="id" value="${a.billID}"/>
                                            <input type="submit" value="Payment"/>
                                        </form>
                                    </td>
                                </c:if>
                                <c:if test="${a.status eq true}">
                                    <td> Have Payed</td>
                                    <td>
                                    </td>
                                </c:if>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
        <body>
            <!--<div class="page_live div_over bg_xam">-->

        </body>

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <%@include file="../components/footer.jsp" %>

    <!--<div id="preloader"></div>-->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="homeAssets/vendor/purecounter/purecounter.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/glightbox/js/glightbox.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/swiper/swiper-bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/php-email-form/validate.js" type="text/javascript"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>
</html>
