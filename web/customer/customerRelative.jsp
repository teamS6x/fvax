<%-- 
    Document   : customerRelative
    Created on : Mar 25, 2022, 12:47:56 PM
    Author     : hoang
--%>

<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %> 
<!DOCTYPE html>
<html>
    <!-- ======= header ======= -->
    <%@include file="../components/header.jsp" %>
    <main id="main">

        <!-- ======= Breadcrumbs Section ======= -->
        <section class="breadcrumbs">
            <div class="container">

                <div class="d-flex justify-content-between align-items-center">
                    <ol>
                        <li><a href="index.html">Home</a></li>
                    </ol>
                </div>

            </div>
        </section><!-- End Breadcrumbs Section -->

        <c:choose>
            <c:when test="${empty requestScope.cusList}">
                <div class="container">
                    <div class="d-flex justify-content-center">
                        <div class="card text-center"style="margin-top: 10px">
                            <div class="card-body " >
                                <h5 class="card-title">No Data</h5>
                                <p class="card-text">There are no data</p>
                                <a href="home" class="btn btn-primary">Home</a>
                            </div>
                        </div>
                    </div>
                </div>
            </c:when>
            <c:otherwise>
                <div class="container">
                    <div class="col-md-12">
                        <table class="table table-bordered table-striped" border="0" >
                            <thead>
                                <tr>
                                    <th>Relative Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.cusList}" var="l">
                                    <tr>
                                        <td><a href="UpcomingReservation?id=${l.customerID}">${l.customerName}</a></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </c:otherwise>
        </c:choose>

    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    <%@include file="../components/footer.jsp" %>

    <!--<div id="preloader"></div>-->
    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="homeAssets/vendor/purecounter/purecounter.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/glightbox/js/glightbox.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/swiper/swiper-bundle.min.js" type="text/javascript"></script>
    <script src="homeAssets/vendor/php-email-form/validate.js" type="text/javascript"></script>

    <!-- Template Main JS File -->
    <script src="assets/js/main.js"></script>

</body>
</html>
