<%-- 
    Document   : header
    Created on : Oct 13, 2021, 7:07:49 PM
    Author     : win
--%>

<%@page import="model.ListPackage"%>
<%@page import="java.util.ArrayList"%>
<%@page import="model.Account"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">

<title>FVAX HOME - Index</title>
<meta content="" name="description">
<meta content="" name="keywords">

<!-- Favicons -->
<link href="common/homeAssets/img/logo.png" rel="icon">
<link href="common/homeAssets/img/logo.png" rel="logo">

<!-- Google Fonts -->
<link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
    rel="stylesheet">

<!-- Vendor CSS Files -->
<link href="common/homeAssets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
<link href="common/homeAssets/vendor/animate.css/animate.min.css" rel="stylesheet">
<link href="common/homeAssets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="common/homeAssets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
<link href="common/homeAssets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
<link href="common/homeAssets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
<link href="common/homeAssets/vendor/remixicon/remixicon.css" rel="stylesheet">
<link href="common/homeAssets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" 
      integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" 
integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
<!-- Template Main CSS File -->
<link href="common/homeAssets/css/style.css" rel="stylesheet" type="text/css"/>
<!-- =======================================================
* Template Name: Medilab - v4.7.1
* Template URL: https://bootstrapmade.com/medilab-free-medical-bootstrap-theme/
* Author: BootstrapMade.com
* License: https://bootstrapmade.com/license/
======================================================== -->

<body>

    <!-- ======= Top Bar ======= -->
    <div id="topbar" class="d-flex align-items-center fixed-top">
        <div class="container d-flex justify-content-between">
            <div class="contact-info d-flex align-items-center">
                <i class="bi bi-envelope"></i> <a href="mailto:haidhhe151032@fpt.edu.vn">haidhhe151032@fpt.edu.vn</a>
                <i class="bi bi-phone"></i> +48963418230
            </div>
            <div class="d-none d-lg-flex social-links align-items-center">
                <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
            </div>
        </div>
    </div>

    <!-- ======= Header ======= -->
    <header id="header" class="fixed-top">
        <div class="container d-flex align-items-center">

            <h1 class="logo me-auto" style="font-size: 100px;"><a href="home">FVAX</a></h1>

            <nav id="navbar" class="navbar order-last order-lg-0">
                <ul>
                    <li><a class="nav-link scrollto" href="home">Home</a></li>
                    <li class="dropdown"><a href="#"><span>Injection History</span> <i class="bi bi-chevron-down"></i></a>
                        <ul>
                            <li><a href="customerreservationcontroller">Yourself</a></li>
                            <li><a href="RelativeController">Your Family</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="#"><span>Upcoming Injection</span> <i class="bi bi-chevron-down"></i></a>
                        <ul>
                            <li><a href="UpcomingReservation?id=user">Yourself</a></li>
                            <li><a href="RelativeListController">Your Family</a></li>
                        </ul>
                    </li>
                    <li class="dropdown"><a href="listPackage"><span>Services</span> <i class="bi bi-chevron-down"></i></a>
                        <ul>
                        </ul>                
                    </li>
                    <li><a class="nav-link scrollto" href="vaccinep">List Vaccine</a></li>
                    <li><a class="nav-link scrollto" href="injection-booking">Book Injection</a></li>
                    <li><a class="nav-link scrollto" href="listdoctorcontroller">Doctors</a></li>
                    <li><a class="nav-link scrollto" href="feedback">FeedBack</a></li>
                    &emsp;
                    <div class="btn-group">
                        <c:choose>
                            <c:when test="${sessionScope.acc==null}">
                                <a  style="font-size: 15px" href="login"> 
                                    Login
                                    <i class="bi bi-person-circle" style="font-size: 15px"></i>
                                </a>
                            </c:when>
                            <c:otherwise>
                                <li class="dropdown"><a href="#"> 
                                        <i class="bi bi-person-circle " style="font-size: 15px; "> ${sessionScope.username}</i></a>
                                    <ul>
                                        <li><a href="customerpf">My Profile</a></li>
                                        <li><a href="changepass">Change Password</a></li>
                                        <li><a class="dropdown-item mr-5" href="showbill">Bills</a></li>
                                        <li>
                                            <div class="dropdown-divider"></div>
                                        </li>
                                        <li><a href="logout">Logout</a></li>

                                    </ul>
                                </li>


                            </c:otherwise>
                        </c:choose>

                    </div>
                </ul>
                <i class="bi bi-list mobile-nav-toggle"></i>
            </nav><!-- .navbar -->
        </div>
    </header><!-- End Header -->
