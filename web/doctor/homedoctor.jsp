<%-- 
    Document   : homeAdmin
    Created on : 23-02-2022, 16:56:56
    Author     : a
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <%@include file="components/headerdoctor.jsp" %>
    
            <div class="container-fluid" style="margin-bottom: 0.5rem;">
                <div class="row">
                    <img class="img-fluid"  src="admin/assets/img/nurse-doctor-medicine.jpg" alt="none"
                         width="100%">
                </div>
            </div>
        
    <%@include file="components/footerdoctor.jsp" %>   
    <div class="sidebar-overlay" data-reff=""></div>
    <script src="admin/assets/js/jquery-3.2.1.min.js"></script>
    <script src="admin/assets/js/popper.min.js"></script>
    <script src="admin/assets/js/bootstrap.min.js"></script>
    <script src="admin/assets/js/jquery.slimscroll.js"></script>
    <script src="admin/assets/js/Chart.bundle.js"></script>
    <script src="admin/assets/js/chart.js"></script>
    <script src="admin/assets/js/app.js"></script>
</body>
</html>
