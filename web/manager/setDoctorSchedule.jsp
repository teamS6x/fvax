<%-- 
    Document   : doctorSchedule
    Created on : Mar 20, 2022, 4:09:46 PM
    Author     : admin
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="model.Doctor"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="model.Reservation"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="components/headermanager.jsp" %>
    <div class="page-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-sm-4 col-3">
                    <h4 class="page-title"></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <form action="setDoctorScheduleController" method="post">
                        <%
                            Reservation reser = (Reservation) request.getAttribute("reser");
                            List<Doctor> doctorName = (List<Doctor>) request.getAttribute("listDoc");
                        %>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-weight: bold">Customer Name</label>
                                    <input class="form-control" type="text" name="custName" value="<%= reser.getCustomerName()%>" readonly/>   
                                    <input type="text" hidden="" name="custId" value="<%= reser.getCustomerId()%>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-weight: bold">Doctor Name</label><br/>
                                    <select name="doctorId">
                                        <%
                                            for (Doctor d : doctorName) {
                                                if (d.getDoctorID() == reser.getDoctorId()) {
                                        %>
                                        <option selected="selected" value="<%= d.getDoctorID()%>"><%= d.getName()%></option>
                                        <%
                                        } else {
                                        %>
                                        <option value="<%= d.getDoctorID()%>"><%= d.getName()%></option>
                                        <%}
                                            }%>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="m-t-20 text-center">
                            <button type="submit" name="submit" class="btn btn-primary submit-btn">Save </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <%@include file="components/footermanager.jsp" %>        
        <div class="sidebar-overlay" data-reff=""></div>
        <script src="admin/assets/js/jquery-3.2.1.min.js"></script>
        <script src="admin/assets/js/popper.min.js"></script>
        <script src="admin/assets/js/bootstrap.min.js"></script>
        <script src="admin/assets/js/jquery.dataTables.min.js"></script>
        <script src="admin/assets/js/dataTables.bootstrap4.min.js"></script>
        <script src="admin/assets/js/jquery.slimscroll.js"></script>
        <script src="admin/assets/js/select2.min.js"></script>
        <script src="admin/assets/js/moment.min.js"></script>
        <script src="admin/assets/js/bootstrap-datetimepicker.min.js"></script>
        <script src="admin/assets/js/app.js"></script>
        <script src="admin/assets/js/activeTaskbar.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">    
        <script>
            $(document).ready(function () {
                $('#mytable').DataTable();
            });
        </script>
    </body>
</html>
