<%-- 
    Document   : editReservationStatus
    Created on : Mar 21, 2022, 9:45:32 AM
    Author     : admin
--%>

<%@page import="model.Bill"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="model.Reservation"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %> 

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="components/headermanager.jsp" %>
    <div class="page-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-sm-4 col-3">
                    <h4 class="page-title"></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <form action="editBillStatus" method="post">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold">Bill ID</label>
                                <input class="form-control" type="text" name="billId" value="${requestScope.bill.billID}" readonly/>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold">Customer Name</label>
                                <input class="form-control" type="text" name="custName" value="${requestScope.bill.customerName}" readonly/>                                
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <c:choose>
                                    <c:when test="${requestScope.bill.packageName eq null}">
                                        <label style="font-weight: bold">Vaccine Name</label>
                                        <input class="form-control" type="text" name="vaccineName" value="${requestScope.bill.vaccineName}" readonly/> 
                                    </c:when>
                                    <c:otherwise>
                                        <label style="font-weight: bold">Package Name</label>
                                        <input class="form-control" type="text" name="vaccineName" value="${requestScope.bill.packageName}" readonly/> 
                                    </c:otherwise>
                                </c:choose>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold">Price</label>
                                <fmt:formatNumber type="number" groupingUsed="true" value="${requestScope.bill.total_Price}" var="b"/>
                                <input class="form-control" type="text" name="price" value="${b}" readonly/>                                
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold">Created Date</label>
                                <input class="form-control" type="text" name="date" value="${requestScope.bill.date}" readonly/>                                
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-weight: bold">Status</label><br/>
                                <c:choose>
                                    <c:when test="${requestScope.bill.status eq true}">
                                        <input type="radio" name="status" value="true" checked/>Have Payed
                                        <input type="radio" name="status" value="false" />Not pay yet
                                    </c:when>
                                    <c:otherwise>
                                        <input type="radio" name="status" value="true" />Have Payed
                                        <input type="radio" name="status" value="false" checked/>Not pay yet
                                    </c:otherwise>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                    <div class="m-t-20 text-center">
                        <button type="submit" name="submit" class="btn btn-primary submit-btn">Save </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <%@include file="components/footermanager.jsp" %>        
    <div class="sidebar-overlay" data-reff=""></div>
    <script src="admin/assets/js/jquery-3.2.1.min.js"></script>
    <script src="admin/assets/js/popper.min.js"></script>
    <script src="admin/assets/js/bootstrap.min.js"></script>
    <script src="admin/assets/js/jquery.dataTables.min.js"></script>
    <script src="admin/assets/js/dataTables.bootstrap4.min.js"></script>
    <script src="admin/assets/js/jquery.slimscroll.js"></script>
    <script src="admin/assets/js/select2.min.js"></script>
    <script src="admin/assets/js/moment.min.js"></script>
    <script src="admin/assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="admin/assets/js/app.js"></script>
    <script src="admin/assets/js/activeTaskbar.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">    
    <script>
        $(document).ready(function () {
            $('#mytable').DataTable();
        });
    </script>
</body>
</html>
