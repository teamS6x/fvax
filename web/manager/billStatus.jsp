<%@page import="model.Bill"%>
<%@page import="model.Reservation"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %> 
<!DOCTYPE html>
<html lang="en">
    <%@include file="components/headermanager.jsp" %>
    <div class="page-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-sm-4 col-3">
                    <h4 class="page-title">All Bill</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-11" style="margin: 0 auto;">
                    <div class="table-responsive">

                        <table class="table table-striped custom-table " id="mytable">
                            <thead>
                                <tr>
                                    <th style="text-align: center;width: 175px">Customer Name</th>
                                    <th style="text-align: center;width: 150px">Vaccine/Package Name</th>
                                    <th style="text-align: center;width: 150px">Price</th>
                                    <th style="text-align: center;width: 150px">Created Date</th>
                                    <th style="text-align: center;width: 150px">Status</th>
                                    <th class="text-right">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.listBill}" var="b">
                                    <tr>
                                        <td style="text-align: center;">${b.customerName}</td>
                                        <c:choose>
                                            <c:when test="${b.packageName eq null}">
                                                <td style="text-align: center;">${b.vaccineName}</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td style="text-align: center;">${b.packageName}</td>
                                            </c:otherwise>
                                        </c:choose>
                                        <td style="text-align: center;">
                                            <fmt:formatNumber type="number" groupingUsed="true" value="${b.total_Price}"/><span> VND</span>
                                        </td>
                                        <td style="text-align: center;">${b.date}</td>
                                        <c:choose>
                                            <c:when test="${b.status eq true}">
                                                <td style="text-align: center;">Have payed</td>
                                            </c:when>
                                            <c:otherwise>
                                                <td style="text-align: center;">Not pay yet</td>
                                            </c:otherwise>
                                        </c:choose>

                                        <td class="text-right">
                                            <div class="dropdown dropdown-action">
                                                <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="editBillStatus?billID=${b.billID}"><i class="fa fa-pencil m-r-5"></i> Change Status</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>


                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
        <%@include file="components/footermanager.jsp" %>        
        <div class="sidebar-overlay" data-reff=""></div>
        <script src="admin/assets/js/jquery-3.2.1.min.js"></script>
        <script src="admin/assets/js/popper.min.js"></script>
        <script src="admin/assets/js/bootstrap.min.js"></script>
        <script src="admin/assets/js/jquery.dataTables.min.js"></script>
        <script src="admin/assets/js/dataTables.bootstrap4.min.js"></script>
        <script src="admin/assets/js/jquery.slimscroll.js"></script>
        <script src="admin/assets/js/select2.min.js"></script>
        <script src="admin/assets/js/moment.min.js"></script>
        <script src="admin/assets/js/bootstrap-datetimepicker.min.js"></script>
        <script src="admin/assets/js/app.js"></script>
        <script src="admin/assets/js/activeTaskbar.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">    
        <script>
             $(document).ready(function () {
                $('#mytable').DataTable({
                    "order": [[ 4, "desc" ]]
                });
            });
        </script>
    </body>
</html>
