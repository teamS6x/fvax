<%@page import="model.Reservation"%>
<%@page import="java.util.List"%>
<%@page import="java.sql.ResultSet"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"  %> 
<!DOCTYPE html>
<html lang="en">
    <%@include file="components/headermanager.jsp" %>
    <div class="page-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-sm-4 col-3">
                    <h4 class="page-title">All Reservation</h4>
                </div>
            </div>

            <div class="row">
                <div class="col-md-11" style="margin: 0 auto;">
                    <div class="table-responsive">
                        <form method="post" action="schedule">
                            <table class="table table-striped custom-table " id="mytable">
                                <thead>
                                    <tr>
                                        <th style="text-align: center;width: 70px">Reservation ID</th>
                                        <th style="text-align: center;width: 175px">Booking Date</th>
                                        <th style="text-align: center">Slot</th>
                                        <th style="text-align: center;width: 150px">Customer Name</th>
                                        <th style="text-align: center;width: 150px">Doctor Name</th>
                                        <th style="text-align: center;width: 150px">Status</th>
                                        <th class="text-right">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        List<Reservation> reservations = (List<Reservation>) request.getAttribute("listRes");
                                        for (Reservation r : reservations) {
                                            String[] date = r.getBookingDate().split("\\s");
                                    %>
                                    <tr>
                                        <td style="text-align: center;"><%= r.getReservationID()%></td>
                                        <td style="text-align: center;"><%= date[0]%></td>
                                        <td style="text-align: center;"><%= r.getSlotID()%></td>
                                        <td style="text-align: center;"><%= r.getCustomerName()%></td>
                                        <td style="text-align: center;"><%= r.getDoctorName()%></td>
                                        <td style="text-align: center;"><%= r.getStatus()%></td>
                                        <td class="text-right">
                                            <div class="dropdown dropdown-action">
                                                <a href="#" class="action-icon dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                                                    <%if (r.getStatus().equals("Success") || r.getStatus().equals("Failed")) {%>

                                                <%} else {%>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="editStatusReser?ReservationID=<%=r.getReservationID()%>"><i class="fa fa-pencil m-r-5"></i> Change Status</a>
                                                </div>
                                                <%}%>

                                            </div>
                                        </td>
                                    </tr>
                                    <%}%>
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="components/footermanager.jsp" %>        
        <div class="sidebar-overlay" data-reff=""></div>
        <script src="admin/assets/js/jquery-3.2.1.min.js"></script>
        <script src="admin/assets/js/popper.min.js"></script>
        <script src="admin/assets/js/bootstrap.min.js"></script>
        <script src="admin/assets/js/jquery.dataTables.min.js"></script>
        <script src="admin/assets/js/dataTables.bootstrap4.min.js"></script>
        <script src="admin/assets/js/jquery.slimscroll.js"></script>
        <script src="admin/assets/js/select2.min.js"></script>
        <script src="admin/assets/js/moment.min.js"></script>
        <script src="admin/assets/js/bootstrap-datetimepicker.min.js"></script>
        <script src="admin/assets/js/app.js"></script>
        <script src="admin/assets/js/activeTaskbar.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">    
        <script>
            $(document).ready(function () {
                $('#mytable').DataTable();
            });
        </script>
    </body>
</html>
