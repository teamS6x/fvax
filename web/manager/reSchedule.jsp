<%-- 
    Document   : editReservationStatus
    Created on : Mar 21, 2022, 9:45:32 AM
    Author     : admin
--%>

<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="model.Reservation"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@include file="components/headermanager.jsp" %>
    <div class="page-wrapper">
        <div class="content">
            <div class="row">
                <div class="col-sm-4 col-3">
                    <h4 class="page-title"></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 offset-lg-2">
                    <form action="ReScheduleController" method="post">
                        <%
                            Reservation reser = (Reservation) request.getAttribute("reser");
                            String[] date = reser.getBookingDate().split("\\s");
                        %>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-weight: bold">Reservation ID</label>
                                    <input class="form-control" type="text" name="reserId" value="<%= reser.getReservationID()%>" readonly/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-weight: bold">Booking Date</label>
                                    <input class="form-control" type="text" name="bookingdate" value="<%= date[0] %>" />                                
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-weight: bold">Slot ID</label>
                                    <input class="form-control" type="text" name="slotId" value="<%= reser.getSlotID()%>" readonly/>                                
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-weight: bold">Customer Name</label>
                                    <input class="form-control" type="text" name="cusId" value="<%= reser.getCustomerName()%>" readonly/>                                
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-weight: bold">Doctor Name</label>
                                    <input class="form-control" type="text" name="cusId" value="<%= reser.getDoctorName()%>" readonly/>                                
                                </div>
                            </div>
                            <div class="col-md-6">

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-weight: bold">Status</label><br/>
                                    <input type="hidden" name="statusold" value="<%=reser.getStatus()%>" />
                                    <input type="radio" name="status" value="Pending" checked/>Pending
                                    <input type="radio" name="status" value="Failed" disabled/>Failed
                                </div>
                            </div>
                        </div>
                        <div class="m-t-20 text-center">
                            <button type="submit" name="submit" class="btn btn-primary submit-btn">Save </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <%@include file="components/footermanager.jsp" %>        
        <div class="sidebar-overlay" data-reff=""></div>
        <script src="admin/assets/js/jquery-3.2.1.min.js"></script>
        <script src="admin/assets/js/popper.min.js"></script>
        <script src="admin/assets/js/bootstrap.min.js"></script>
        <script src="admin/assets/js/jquery.dataTables.min.js"></script>
        <script src="admin/assets/js/dataTables.bootstrap4.min.js"></script>
        <script src="admin/assets/js/jquery.slimscroll.js"></script>
        <script src="admin/assets/js/select2.min.js"></script>
        <script src="admin/assets/js/moment.min.js"></script>
        <script src="admin/assets/js/bootstrap-datetimepicker.min.js"></script>
        <script src="admin/assets/js/app.js"></script>
        <script src="admin/assets/js/activeTaskbar.js" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.min.css">    
        <script>
            $(document).ready(function () {
                $('#mytable').DataTable();
            });
        </script>
    </body>
</html>
