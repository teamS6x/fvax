$(document).ready(function() {
	
	// Bar Chart
        
        var dataset = document.getElementById('customerDataset').value;
        var dataset2 = document.getElementById('customerDataset2').value;
        
        var dataset3 = document.getElementById('customerDataset3').value;
        var dataset4 = document.getElementById('customerDataset4').value;

        const thisYearBookingNumberDataset = dataset.split(',');
        const lastYearBookingNumberDataset = dataset2.split(',');
        const thisYearMonthlyProfit = dataset3.split(',');
        const lastYearMonthlyProfit = dataset4.split(',');
        
	var barChartData = {
		labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
		datasets: [{
			label: 'Current Year Profit',
			backgroundColor: 'rgba(0, 158, 251, 0.5)',
			borderColor: 'rgba(0, 158, 251, 1)',
			borderWidth: 1,
			data: thisYearMonthlyProfit
		}, {
			label: 'Last Year Profit',
			backgroundColor: 'rgba(255, 188, 53, 0.5)',
			borderColor: 'rgba(255, 188, 53, 1)',
			borderWidth: 1,
			data: lastYearMonthlyProfit
		}]
	};

	var ctx = document.getElementById('bargraph').getContext('2d');
	window.myBar = new Chart(ctx, {
		type: 'bar',
		data: barChartData,
		options: {
			responsive: true,
			legend: {
				display: false,
			}
		}
	});

	// Line Chart

	var lineChartData = {
		labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
		datasets: [{
			label: "Current Year Number",
			backgroundColor: "rgba(0, 158, 251, 0.5)",
			data:  thisYearBookingNumberDataset
		}, {
		label: "Last Year Number",
		backgroundColor: "rgba(255, 188, 53, 0.5)",
		data: lastYearBookingNumberDataset
		}]
	};
	
	var linectx = document.getElementById('linegraph').getContext('2d');
	window.myLine = new Chart(linectx, {
		type: 'line',
		data: lineChartData,
		options: {
			responsive: true,
			legend: {
				display: false,
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			}
		}
	});
	
	// Bar Chart 2
	
    barChart();
    
    $(window).resize(function(){
        barChart();
    });
    
    function barChart(){
        $('.bar-chart').find('.item-progress').each(function(){
            var itemProgress = $(this),
            itemProgressWidth = $(this).parent().width() * ($(this).data('percent') / 100);
            itemProgress.css('width', itemProgressWidth);
        });
    };
});