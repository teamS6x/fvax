/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import model.Bill;

/**
 *
 * @author dattrinh
 */
public class DaoBill extends DBContext{
    PreparedStatement stm=null;
    ResultSet rs=null;
    public void CloseConnection() throws SQLException{
        if (rs !=null) {
                rs.close();
            }
        if (stm!=null){
                stm.close();
            } 
    }
    public void createBill1(Bill b){
        try{
            String  sql = "Insert into Bill(Total_price,CustomerID,vaccineID,status"
                    + ",createdDate) values(?,?,?,?,?)";
            stm = conn.prepareStatement(sql);
            stm.setDouble(1, b.getTotal_Price());
            stm.setInt(2,b.getCustomerID());
            stm.setInt(3,b.getVaccineID());
            stm.setBoolean(4, b.status);
            LocalDate ldate = LocalDate.now();
            Date date = Date.valueOf(ldate);
            stm.setDate(5, date);
            stm.execute();
            CloseConnection();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    public void createBill2(Bill b){
        try{
            String  sql = "Insert into Bill(Total_price,CustomerID,PackageID,status"
                    + ",createdDate) values(?,?,?,?,?)";
            stm = conn.prepareStatement(sql);
            stm.setDouble(1, b.getTotal_Price());
            stm.setInt(2,b.getCustomerID());
            stm.setInt(3,b.getPackageID());
            stm.setBoolean(4, b.status);
            LocalDate ldate = LocalDate.now();
            Date date = Date.valueOf(ldate);
            stm.setDate(5, date);
            stm.execute();
            CloseConnection();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    public void changeStatus(int id){
        try{
            String sql="Update Bill set status =? where BillID=?";
            stm = conn.prepareStatement(sql);
            stm.setInt(1, 1);
            stm.setInt(2, id);
            stm.execute();
            CloseConnection();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    public List<Bill> getBillByCustomerID(int cusID){
        List <Bill> list = new ArrayList<>();
        try{
            String sql = "Select B.*,v.vaccineName from Bill b,Account a, Customer c,Vaccine v \n" +
                          "where a.AccountID =? and c.AccountID=a.AccountID and b.customerID = c.customerID \n" +
                          "and v.vaccineId= b.vaccineId ";
            stm = conn.prepareStatement(sql);
            stm.setInt(1, cusID);
            rs = stm.executeQuery();
            while (rs.next()){
                Bill b = new Bill(rs.getInt("BillID"),rs.getFloat("Total_price")
                        ,rs.getInt("CustomerID"), rs.getInt("PackageID"), rs.getInt("VaccineID"));
                b.status = rs.getBoolean("status");
                b.setDate(rs.getDate("createdDate"));
                b.setVaccineName(rs.getString("vaccineName"));
                list.add(b);
            }
            sql = "Select B.*,p.PackageName from Bill b,Account a, Customer c,VaccinePackage p \n" +
"where a.AccountID =? and c.AccountID=a.AccountID and b.customerID = c.customerID \n" +
"and p.PackageID=b.PackageID";
            stm = conn.prepareStatement(sql);
            stm.setInt(1, cusID);
            rs = stm.executeQuery();
            while (rs.next()){
                Bill b = new Bill(rs.getInt("BillID"),rs.getFloat("Total_price")
                        ,rs.getInt("CustomerID"), rs.getInt("PackageID"), rs.getInt("VaccineID"));
                b.status = rs.getBoolean("status");
                b.setDate(rs.getDate("createdDate"));
                b.setPackageName(rs.getString("PackageName"));
                list.add(b);
            }
            CloseConnection();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return list;
    }
    public List <Bill>getAll(){
        List <Bill> list = new ArrayList<>();
        try{
            String sql = "Select * from Bill";
            stm = conn.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()){
                Bill b = new Bill(rs.getInt("BillID"),rs.getFloat("Total_price")
                        ,rs.getInt("CustomerID"), rs.getInt("PackageID"), rs.getInt("VaccineID"));
                b.status = rs.getBoolean("status");
                b.setDate(rs.getDate("createdDate"));
                list.add(b);
            }
            CloseConnection();
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return list;
    }
}
