package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Bill;
import model.Doctor;
import model.ListPackage;
import model.Reservation;

public class DaoManager extends DBContext {

    public void updateDoctorIdInReservation(int custId, int doctorId) {
        try {
            PreparedStatement st = conn.prepareStatement("update Reservation set DoctorID = ? where CustomerId = ? and Status = 'Pending'");
            st.setInt(1, doctorId);
            st.setInt(2, custId);
            st.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DaoManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateStatusInReservation(String status, int reservationId) {
        try {
            PreparedStatement st = conn.prepareStatement("update Reservation set Status = ? where ReservationID = ?");
            st.setString(1, status);
            st.setInt(2, reservationId);
            st.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DaoManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Reservation> getAllReservation() {
        try {
            String sql = "select Reservation.ReservationID, Doctor.Name as DoctorName,Doctor.DoctorID,\n"
                    + "Customer.CustomerName, Customer.CustomerID, SlotID, BookingDate, Reservation.Status\n"
                    + "from Reservation\n"
                    + "inner join Doctor on Doctor.DoctorID = Reservation.DoctorID\n"
                    + "inner join Customer on Customer.CustomerID = Reservation.CustomerId\n"
                    + "where BookingDate between DATEADD (DAY, -15, GETDATE()) and DATEADD (DAY, 15, GETDATE())\n"
                    + "and Reservation.Status != 'Success'";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Reservation> list = new ArrayList<>();
            while (rs.next()) {
                Reservation r = new Reservation(
                        rs.getInt("ReservationID"),
                        rs.getString("DoctorName"),
                        rs.getInt("DoctorID"),
                        rs.getString("CustomerName"),
                        rs.getInt("CustomerID"),
                        rs.getString("BookingDate"),
                        rs.getInt("SlotID"),
                        rs.getString("Status"));
                list.add(r);
            }
            return list;
        } catch (SQLException e) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;

    }

    public List<Reservation> getAllReservationByCustId() {
        try {
            String sql = "select distinct Doctor.Name as DoctorName,Reservation.DoctorID, Customer.CustomerName, Customer.CustomerID, SlotID, BookingDate, \n"
                    + "Reservation.Status, Reservation.ReservationId\n"
                    + "from Reservation\n"
                    + "left join Doctor on Doctor.DoctorID = Reservation.DoctorID\n"
                    + "inner join Customer on Customer.CustomerID = Reservation.CustomerId\n"
                    + "where Reservation.Status != 'Success'\n"
                    + "order by Reservation.BookingDate ASC";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Reservation> list = new ArrayList<>();
            while (rs.next()) {
                Reservation r = new Reservation(
                        rs.getString("DoctorName"),
                        rs.getInt("DoctorID"),
                        rs.getString("CustomerName"),
                        rs.getInt("CustomerID"),
                        rs.getInt("SlotID"),
                        rs.getString("BookingDate"),
                        rs.getString("Status"));
                r.setReservationID(rs.getInt("ReservationId"));
                list.add(r);
            }
            return list;
        } catch (SQLException e) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    public int getVaccineIdByReservationId(int id) {
        try {
            String sql = "select VaccineId from Reservation where ReservationId = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            Logger.getLogger(DaoManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public Reservation getReservationById(int reservationId) {
        Reservation reser = null;
        String sql = "select Reservation.ReservationID, Doctor.Name as DoctorName,Doctor.DoctorID, \n"
                + "Customer.CustomerName, Customer.CustomerID, SlotID, BookingDate, Reservation.Status\n"
                + "from Reservation \n"
                + "inner join Doctor on Doctor.DoctorID = Reservation.DoctorID\n"
                + "inner join Customer on Customer.CustomerID = Reservation.CustomerId \n"
                + "where ReservationID = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, reservationId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                reser = new Reservation(
                        rs.getInt("ReservationID"),
                        rs.getString("DoctorName"),
                        rs.getInt("DoctorID"),
                        rs.getString("CustomerName"),
                        rs.getInt("CustomerID"),
                        rs.getString("BookingDate"),
                        rs.getInt("SlotID"),
                        rs.getString("Status"));
            }
        } catch (SQLException e) {
        }
        return reser;
    }

    public Reservation getReservationByCustID(int custID) {
        Reservation reser = null;
        String sql = "select distinct Reservation.DoctorID, \n"
                + "Customer.CustomerName, Customer.CustomerID, SlotID, BookingDate, Reservation.Status\n"
                + "from Reservation \n"
                + "inner join Customer on Customer.CustomerID = Reservation.CustomerId \n"
                + "where Reservation.CustomerId = ? and Reservation.Status = 'Pending'";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, custID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                reser = new Reservation(
                        rs.getInt("DoctorID"),
                        rs.getString("CustomerName"),
                        rs.getInt("CustomerID"),
                        rs.getInt("SlotID"),
                        rs.getString("BookingDate"),
                        rs.getString("Status"));
            }
        } catch (SQLException e) {
        }
        return reser;
    }

    public List<Doctor> getAllDoctor() {
        try {
            String sql = "select * from Doctor";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Doctor> list = new ArrayList<>();
            while (rs.next()) {
                Doctor doctor = new Doctor(
                        rs.getInt("DoctorID"),
                        rs.getString("Information"),
                        rs.getString("Phone"),
                        rs.getInt("Exp_year"),
                        rs.getShort("Gender"),
                        rs.getString("Email"),
                        rs.getString("Name"),
                        rs.getString("Address"),
                        rs.getInt("AccountID"));
                list.add(doctor);
            }
            return list;
        } catch (SQLException e) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    public Doctor getDoctorNameById(int doctorId) {
        Doctor doctor = null;
        String sql = "select * from Doctor where DoctorID = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, doctorId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                doctor = new Doctor(
                        rs.getInt("DoctorID"),
                        rs.getString("Information"),
                        rs.getString("Phone"),
                        rs.getInt("Exp_year"),
                        rs.getShort("Gender"),
                        rs.getString("Email"),
                        rs.getString("Name"),
                        rs.getString("Address"),
                        rs.getInt("AccountID"));
            }
        } catch (SQLException e) {
        }
        return doctor;
    }

    public List<Bill> getAllBills() {
        try {
            String sql = "select BillId, Bill.CustomerId, Customer.CustomerName, VaccinePackage.PackageName, Vaccine.VaccineName, Total_price, Bill.Status, CreatedDate\n"
                    + "from Bill join Customer on Bill.CustomerId = Customer.CustomerId\n"
                    + "left join VaccinePackage on VaccinePackage.PackageId = Bill.PackageId\n"
                    + "left join Vaccine on Vaccine.VaccineId = Bill.VaccineId\n"
                    + "where Bill.Status = 0";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Bill> list = new ArrayList<>();
            while (rs.next()) {
                Bill bill = new Bill(
                        rs.getInt("BillId"),
                        rs.getInt("CustomerId"),
                        rs.getString("CustomerName"),
                        rs.getString("PackageName"),
                        rs.getString("VaccineName"),
                        rs.getFloat("Total_price"),
                        rs.getBoolean("Status"),
                        rs.getDate("CreatedDate"));
                list.add(bill);
            }
            return list;
        } catch (Exception e) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }

    // co dung
    public Bill getBillById(int billId) {
        Bill bill = null;
        String sql = "select BillId, Bill.CustomerId, Customer.CustomerName, VaccinePackage.PackageName, Vaccine.VaccineName, \n"
                + "Total_price, Bill.Status, CreatedDate\n"
                + "from Bill join Customer on Bill.CustomerId = Customer.CustomerId\n"
                + "left join VaccinePackage on VaccinePackage.PackageId = Bill.PackageId\n"
                + "left join Vaccine on Vaccine.VaccineId = Bill.VaccineId\n"
                + "where Bill.BillId = ?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, billId);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                bill = new Bill(
                        rs.getInt("BillId"),
                        rs.getInt("CustomerId"),
                        rs.getString("CustomerName"),
                        rs.getString("PackageName"),
                        rs.getString("VaccineName"),
                        rs.getFloat("Total_price"),
                        rs.getBoolean("Status"),
                        rs.getDate("CreatedDate"));
            }
        } catch (SQLException e) {
        }
        return bill;
    }

    public void updateStatusInBill(boolean status, int billId) {
        try {
            PreparedStatement st = conn.prepareStatement("update Bill set Status = ? where BillId = ?");
            st.setBoolean(1, status);
            st.setInt(2, billId);
            st.execute();
        } catch (SQLException ex) {
            Logger.getLogger(DaoManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        DaoManager dao = new DaoManager();
//        DaoBill daoBill = new DaoBill();
//        List<Bill> list = new ArrayList<>();
//        list = dao.getAllBills();
//        for (Bill r : list) {
//            System.out.println(r);
//        }
        //Reservation reser = dao.getReservationById(1);
//        Doctor doc = dao.getDoctorNameById(1);
//        System.out.println(doc);
        //dao.updateStatusInReservation("failed", 40);
//        Reservation reser = dao.getReservationByCustID(1);
//        System.out.println(reser);
        //dao.updateDoctorIdInReservation(1, 1);

//            dao.updateStatusInBill(false, 2);
//            Bill bill = dao.getBillById(2);
//        System.out.println(bill);
        List<Reservation> list = new ArrayList<>();
        list = dao.getAllReservationByCustId();
        for (Reservation reservation : list) {
            System.out.println(reservation);
        }
    }
}
