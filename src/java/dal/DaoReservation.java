/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Reservation;

/**
 *
 * @author hoang
 */
public class DaoReservation extends DBContext {

    PreparedStatement ps = null;
    ResultSet rs = null;

    public String getLatestReservationStatus(int id) {
        String sql = "Select Top 1 Status from Reservation where CustomerId = ?"
                + " Order by ReservationID desc";
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                String status = rs.getString("Status");
                return status;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return "none";
    }

    public void createReservation(Date date, int slot, int cus, int vaccineId) {
        String sql = "Insert into Reservation (VaccineId, BookingDate, SlotID, Status, CustomerId)"
                + " values (?,?, ?, 'Pending', ?)";
        try {
            ps = conn.prepareStatement(sql);
            ps.setInt(1, vaccineId);
            ps.setDate(2, date);
            ps.setInt(3, slot);
            ps.setInt(4, cus);
            ps.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public int getNumberOfSuccessfulReservation() {
        String sql = "SELECT COUNT(reservationId)\n"
                + "from Reservation\n"
                + "where Status = 'Success'";
        try {
            int numberOfSuccessfullReservation = 0;
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                numberOfSuccessfullReservation = rs.getInt(1);
            }
            return numberOfSuccessfullReservation;
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    public ResultSet GetReservationByIdForDoctor(int id) {
        String sql = "select Vaccine.image,Vaccine.vaccineName,Reservation.BookingDate,Reservation.SlotID,Doctor.Name,Reservation.Status,Slot.BeginTime,\n"
                + "Slot.EndTime,Reservation.VaccineId,Reservation.CustomerId,Customer.CustomerName,Reservation.Detail,Reservation.HealthTracking\n"
                + "from ((((Reservation join Vaccine on Reservation.VaccineId=Vaccine.vaccineId)\n"
                + "join Doctor on Reservation.DoctorID =Doctor.DoctorID)join Slot on Reservation.SlotID = Slot.SlotID)\n"
                + "join Customer on Reservation.CustomerId = Customer.CustomerID) where Reservation.CustomerId = ? "
                + "ORDER BY Reservation.status ASC";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet GetReservationById(int id) {
        String sql = "select Vaccine.image,Vaccine.vaccineName,Reservation.BookingDate,Reservation.SlotID,Doctor.Name,Reservation.Status,Slot.BeginTime,\n"
                + "Slot.EndTime,Reservation.VaccineId,Reservation.CustomerId,Customer.CustomerName,Reservation.Detail,Reservation.DoctorID,Reservation.ReservationId,Reservation.HealthTracking\n"
                + "from ((((Reservation join Vaccine on Reservation.VaccineId=Vaccine.vaccineId)\n"
                + "join Doctor on Reservation.DoctorID =Doctor.DoctorID)join Slot on Reservation.SlotID = Slot.SlotID)\n"
                + "join Customer on Reservation.CustomerId = Customer.CustomerID) "
                + "where Reservation.CustomerId = ? and Reservation.status = 'Success' "
                + "order by Reservation.ReservationId Desc";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet GetCusNameByDoctorId(int doctorid) {
        String sql = "SELECT DISTINCT Customer.CustomerName FROM (Reservation join Customer on Reservation.CustomerId = Customer.CustomerID) where Reservation.DoctorID = ? and Customer.status =1 ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, doctorid);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet GetVaccineDetailByVaccineIdCusId(int vaccineid, int customerid) {
        String sql = "select Vaccine.image,Vaccine.vaccineName,Reservation.BookingDate,Reservation.SlotID,Doctor.Name,Reservation.Status\n"
                + ",Slot.BeginTime,Slot.EndTime,Reservation.VaccineId,Reservation.CustomerId,Customer.CustomerName,Reservation.Detail\n"
                + "from ((((Reservation join Vaccine on Reservation.VaccineId=Vaccine.vaccineId)\n"
                + "join Doctor on Reservation.DoctorID =Doctor.DoctorID)join Slot on Reservation.SlotID = Slot.SlotID)join Customer\n"
                + "on Reservation.CustomerId=Customer.CustomerID) where Reservation.CustomerId = ? AND Reservation.VaccineId =?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, customerid);
            ps.setInt(2, vaccineid);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public ResultSet GetVaccineDetailByVaccineIdCusIdResId(int vaccineid, int customerid, int ResId) {
        String sql = "select Vaccine.image,Vaccine.vaccineName,Reservation.BookingDate,Reservation.SlotID,Doctor.Name,Reservation.Status\n"
                + ",Slot.BeginTime,Slot.EndTime,Reservation.VaccineId,Reservation.CustomerId,Customer.CustomerName,Reservation.Detail,Reservation.ReservationId,Reservation.HealthTracking\n"
                + "from ((((Reservation join Vaccine on Reservation.VaccineId=Vaccine.vaccineId)\n"
                + "join Doctor on Reservation.DoctorID =Doctor.DoctorID)join Slot on Reservation.SlotID = Slot.SlotID)join Customer\n"
                + "on Reservation.CustomerId=Customer.CustomerID) where Reservation.CustomerId = ? AND Reservation.VaccineId =? AND Reservation.ReservationId=?";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, customerid);
            ps.setInt(2, vaccineid);
            ps.setInt(3, ResId);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    public void UpdateReservationByDoctor(int vacid, int cusid, String status, String detail) {
        String sql = "update Reservation set Status = ? , Detail= ?  from ((((Reservation join Vaccine on Reservation.VaccineId=Vaccine.vaccineId)\n"
                + "join Doctor on Reservation.DoctorID =Doctor.DoctorID)join Slot on Reservation.SlotID = Slot.SlotID)join Customer\n"
                + "on Reservation.CustomerId=Customer.CustomerID) where Reservation.CustomerId = ? AND Reservation.VaccineId = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, status);
            pre.setString(2, detail);
            pre.setInt(4, vacid);
            pre.setInt(3, cusid);
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void UpdateReservationByCustomer(int vacid, int cusid, String detail,int resid) {
        String sql = "update Reservation set HealthTracking= ?  from ((((Reservation join Vaccine on Reservation.VaccineId=Vaccine.vaccineId)\n"
                + "join Doctor on Reservation.DoctorID =Doctor.DoctorID)join Slot on Reservation.SlotID = Slot.SlotID)join Customer\n"
                + "on Reservation.CustomerId=Customer.CustomerID) where Reservation.CustomerId = ? AND Reservation.VaccineId = ? AND Reservation.ReservationId = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, detail);
            pre.setInt(3, vacid);
            pre.setInt(2, cusid);
            pre.setInt(4, resid);
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void changeStatusToFailedByCusID(int CustomerID) {
        String sql = "update Reservation set status = 'Failed' where CustomerId = ? and status = 'Pending'";
        try {
            ResultSet rs = new DaoCustomer().GetSignedIdByCustomerId(CustomerID);
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, CustomerID);
            pre.executeUpdate();
            while (rs.next()) {
                pre = conn.prepareStatement(sql);
                pre.setInt(1, rs.getInt(1));
                pre.executeUpdate();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public HashMap<Integer, String> getNumberOfBookingsPerMonth(int increment) {
        String sql = "select MONTH(BookingDate) as month , count(*) AS total\n"
                + "from Reservation\n"
                + "where YEAR(BookingDate) = year( DATEADD(year, ?, GetDate()))\n"
                + "group by MONTH(BookingDate)";
        try {
            HashMap<Integer, String> map = new HashMap<>();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, increment);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                map.put(rs.getInt(1), rs.getString(2));
            }
            return map;

        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public HashMap<Integer, String> getMonthlyProfit(int increment) {
        String sql = "select MONTH(createdDate) as 'month', sum(Total_price) AS total\n"
                + "from Bill\n"
                + "where YEAR(createdDate) = year( DATEADD(year, ?, GetDate()))\n"
                + "group by MONTH(createdDate)";
        try {
            HashMap<Integer, String> map = new HashMap<>();
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, increment);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                map.put(rs.getInt(1), rs.getString(2));
            }
            return map;

        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<Reservation> getUpcomingReservation(int id) {
        String sql = "select Vaccine.vaccineId, Vaccine.image,Vaccine.vaccineName,Reservation.BookingDate,Doctor.Name, Doctor.DoctorID, Slot.BeginTime,\n"
                + "Slot.EndTime\n"
                + "from Reservation\n"
                + "join Vaccine on Reservation.VaccineId=Vaccine.vaccineId\n"
                + "left join Doctor on Reservation.DoctorID =Doctor.DoctorID \n"
                + "join Slot on Reservation.SlotID = Slot.SlotID\n"
                + "join Customer on Reservation.CustomerId = Customer.CustomerID \n"
                + "where Reservation.CustomerId = ? and Reservation.Status = 'Pending'\n"
                + "order by ReservationID asc";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            List<Reservation> list = new ArrayList<>();
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            String date;
            while (rs.next()) {
                date = dateFormat.format(rs.getDate(4));
                Reservation r = new Reservation(
                rs.getInt(1),
                rs.getString(2),
                rs.getString(3),    
                date,
                rs.getString(5),
                rs.getInt(6),
                rs.getString(7),
                rs.getString(8)
                );
                list.add(r);
            }
            return list;

        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int GetDoctorIdFromreservationByCustomerId(int id) {
        String query = "select DISTINCT DoctorID from Reservation where CustomerId=?";
        try {
            //conn = new DBContext().getConnection();
            ps = conn.prepareStatement(query);
            ps.setInt(1, id);
            rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;

    }

    public static void main(String[] args) {
        DaoReservation dao = new DaoReservation();
        int doctorid = dao.GetDoctorIdFromreservationByCustomerId(17);
        System.out.println(doctorid);
    }

    public int getVaccineIdByReservationId(int id) {
        try {
            String sql = "select VaccineId from Reservation where ReservationId = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }
    
    public void reSchedule(int reserID, String bookingdate, String status) {
        String sql = "update Reservation set status = ?,BookingDate =? where ReservationId = ? ";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, status);
            pre.setString(2, bookingdate);
            pre.setInt(3, reserID);
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void reScheduleForAnotherReser(int cusid, java.sql.Date datenew, java.sql.Date dateold) {
        String sql = "update Reservation set BookingDate = BookingDate + (? - ?) where CustomerId = ? and status = 'Pending'";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setDate(1, datenew);
            pre.setDate(2, dateold);
            pre.setInt(3, cusid);
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoReservation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
