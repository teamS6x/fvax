/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dal;

import java.sql.Connection;
import model.Vaccine;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ListPackage;

/**
 *
 * @author a
 */
public class DaoVaccine extends DBContext {

    public void addVaccine(Vaccine vax) {
        String sql = "insert into Vaccine(vaccineName,vaccinePrice,vaccineOrigin,vaccineDetail,quantity,image) values (?,?,?,?,?,?)";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, vax.getName());
            pre.setFloat(2, vax.getPrice());
            pre.setString(3, vax.getOrigin());
            pre.setString(4, vax.getDetail());
            pre.setInt(5, vax.getQuantity());
            pre.setString(6, vax.getImage());
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void deleteVaccine(int VaccineID) {
        try {
            String sql = "delete from Vaccine where vaccineId = '" + VaccineID + "'";
            Statement state = conn.createStatement();
            state.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateVaccine(Vaccine vax, int VaccineID) {
        String sql = "update Vaccine set vaccineName=?,vaccinePrice=?,vaccineOrigin=?,vaccineDetail=?,quantity=? where vaccineId=?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setString(1, vax.getName());
            pre.setFloat(2, vax.getPrice());
            pre.setString(3, vax.getOrigin());
            pre.setString(4, vax.getDetail());
            pre.setInt(5, vax.getQuantity());
            pre.setInt(6, VaccineID);
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<Vaccine> getall() {
        try {
            String sql = "select * from vaccine";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Vaccine> list = new ArrayList<>();
            while (rs.next()) {
                Vaccine v = new Vaccine(
                        rs.getString(2),
                        rs.getFloat(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getInt(7));
                list.add(v);
            }
            return list;
        } catch (Exception e) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;

    }

    public ResultSet getAllPaging(int pageIndex) {
        String sql = "select * from vaccine where status = 1 order by vaccineId OFFSET ? ROWS FETCH NEXT 20 ROWS ONLY;";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, (pageIndex - 1) * 20);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet getAllPagingByText(int pageIndex, String txt) {
        String sql = "select * from vaccine where vaccineName like ? order by vaccineId "
                + "OFFSET ? ROWS FETCH NEXT 20 ROWS ONLY ;";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + txt + "%");
            ps.setInt(2, (pageIndex - 1) * 20);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet getAllByText(String txt) {
        String sql = "select * from vaccine where vaccineName like ? ";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + txt + "%");
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ResultSet GetVaccineByID(int vaccineID) {
        try {
            //mo ket noi
            String sql = "select * from Vaccine where vaccineId = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, vaccineID);
            ResultSet rsGetVaccineByID = ps.executeQuery();
            return rsGetVaccineByID;
        } catch (Exception ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public float GetpriceByID(int vaccineID) {
        try {
            //mo ket noi
            String sql = "select * from Vaccine where vaccineId = ?";
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, vaccineID);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getFloat("vaccinePrice");
            }
        } catch (Exception ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public ResultSet GetAllVaccine() {
        try {
            //mo ket noi
            String sql = "select * from Vaccine";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            return rs;
        } catch (Exception ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int totalvaccines() {
        try {
            String sql = "select count(*) from vaccine";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return rs.getInt(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace(System.out);
        }
        return 0;
    }

    public ArrayList<ListPackage> getListPackage() {
        ArrayList<ListPackage> list = new ArrayList<>();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery("SELECT Vaccine.vaccineId,VaccinePackage.PackageID,PackageName,vaccineName,vaccineOrigin,vaccinePrice,vaccineDetail,Detail \n"
                    + "FROM Vaccine inner join PackageDetail ON Vaccine.vaccineId = PackageDetail.vaccineId \n"
                    + "inner join  VaccinePackage ON VaccinePackage.PackageID = PackageDetail.PackageID\n"
                    + "where VaccinePackage.Status = 1");
            while (rs.next()) {
                ListPackage listPackage = new ListPackage();
                listPackage.setVaccineId(rs.getInt("vaccineId"));
                listPackage.setPackageId(rs.getInt("PackageID"));
                listPackage.setPackageName(rs.getString("PackageName"));
                listPackage.setVaccineName(rs.getString("vaccineName"));
                listPackage.setVaccineOrigin(rs.getString("vaccineOrigin"));
                listPackage.setVaccinePrice(rs.getFloat("vaccinePrice"));
                listPackage.setVaccineDetail(rs.getString("vaccineDetail"));
                listPackage.setPackageDetail(rs.getString("Detail"));
                list.add(listPackage);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Vaccine> getListAllVaccine() {
        List<Vaccine> list = new ArrayList<>();
        try {
            //mo ket noi
            String sql = "select * from Vaccine where status = 1 and quantity >0";
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Vaccine v = new Vaccine(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getFloat(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6),
                        rs.getInt(7)
                );
                list.add(v);
            }
            return list;
        } catch (Exception ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    //test
    public static void main(String[] args) {
        DaoVaccine dao = new DaoVaccine();
        int count = dao.totalvaccines();
        System.out.println(dao.getPackagePriceById(101));
    }

    public void changeStatus(int VaccineID) {
        String sql = "update Vaccine set status = 0 where vaccineId = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, VaccineID);
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int getStatusByVaccineID(int VaccineID) {
        String sql = "select status from Vaccine where vaccineId = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, VaccineID);
            ResultSet rs1 = pre.executeQuery();
            while (rs1.next()) {
                return rs1.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoDoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public Vaccine getBestSellingVaccine() {
        String sql = "select top 1 b.vaccineId, v.vaccineName, count(b.vaccineId)\n"
                + "From Bill b join Vaccine v \n"
                + "On b.vaccineId = v.vaccineId\n"
                + "where b.[status] = 1\n"
                + "group by b.vaccineId, v.vaccineName\n"
                + "order by count(b.vaccineId) desc";

        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            Vaccine v = new Vaccine();
            while (rs.next()) {
                v.setId(rs.getInt(1));
                v.setName(rs.getString(2));
                v.setTotal(rs.getInt(3));
            }
            return v;
        } catch (SQLException ex) {
            Logger.getLogger(DaoDoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public float getVaccinePriceById(int id) {
        String sql = "select vaccinePrice\n"
                + "from Vaccine\n"
                + "where vaccineId = ?";
        try {
            float price = 0;
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                price = rs.getFloat(1);
            }
            return price;
        } catch (SQLException ex) {
            Logger.getLogger(DaoDoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public float getPackagePriceById(int id) {
        String sql = "select PackagePrice\n"
                + "from VaccinePackage\n"
                + "where PackageID = ?";
        try {
            float price = 0;
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                price = rs.getFloat(1);
            }
            return price;
        } catch (SQLException ex) {
            Logger.getLogger(DaoDoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public int getQuantityByVaccineID(int VaccineID) {
        String sql = "select quantity from Vaccine where vaccineId = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, VaccineID);
            ResultSet rs1 = pre.executeQuery();
            while (rs1.next()) {
                return rs1.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoDoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
        return -1;
    }

    public void createBill(int customerId, int vaccineId, float price) {
        String sql = "Insert into Bill (CustomerID, vaccineId, Total_price, status, createdDate)\n"
                + "values (?, ?, ?, 0, GETDATE())";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, customerId);
            ps.setInt(2, vaccineId);
            ps.setFloat(3, price);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoDoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void createBillPackage(int customerId, int packageId, float price) {
        String sql = "Insert into Bill (CustomerID, PackageId, Total_price, status, createdDate)\n"
                + "values (?, ?, ?, 0, GETDATE())";
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.setInt(1, customerId);
            ps.setInt(2, packageId);
            ps.setFloat(3, price);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoDoctor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void UpdateQuantity(int vacid) {
        String sql = "update Vaccine set quantity = (quantity + 1) where vaccineId = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, vacid);
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public void UpdateQuantitySubtract(int vacid) {
        String sql = "update Vaccine set quantity = (quantity - 1) where vaccineId = ?";
        try {
            PreparedStatement pre = conn.prepareStatement(sql);
            pre.setInt(1, vacid);
            pre.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoVaccine.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
