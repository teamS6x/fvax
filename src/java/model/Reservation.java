/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author a
 */
public class Reservation {
    private int reservationID;
    private String bookingDate;
    private int slotID;
    private String status;
    private int customerId;
    private int vaccineId;
    private int doctorId;
    private String vaccineImage;
    private String vaccineName;
    private String doctorName;
    private String slotBegin;
    private String slotEnd;
    private String detail;
    private String customerName;
    
    public Reservation() {
    }

    public Reservation(int vaccineId, String vaccineImage, String vaccineName, String bookingDate,  String doctorName, int doctorId,  String slotBegin, String slotEnd) {
        this.bookingDate = bookingDate;
        this.vaccineId = vaccineId;
        this.vaccineImage = vaccineImage;
        this.vaccineName = vaccineName;
        this.doctorName = doctorName;
        this.slotBegin = slotBegin;
        this.slotEnd = slotEnd;
        this.doctorId = doctorId;
    }

    public Reservation(int reservationID, String bookingDate, int slotID, String status, int customerId, int doctorId, int vaccineId, String detail) {
        this.reservationID = reservationID;
        this.bookingDate = bookingDate;
        this.slotID = slotID;
        this.status = status;
        this.customerId = customerId;
        this.vaccineId = vaccineId;
        this.doctorId = doctorId;
        this.detail = detail;
    }
    
    public Reservation(int reservationID, String bookingDate, int slotID, String status, int customerId, int vaccineId) {
        this.reservationID = reservationID;
        this.bookingDate = bookingDate;
        this.slotID = slotID;
        this.status = status;
        this.customerId = customerId;
        this.vaccineId = vaccineId;
    }

    public Reservation(int reservationID, String bookingDate, int slotID) {
        this.reservationID = reservationID;
        this.bookingDate = bookingDate;
        this.slotID = slotID;
    }
    public Reservation( String bookingDate, int slotID) {
        this.bookingDate = bookingDate;
        this.slotID = slotID;
    }

    public Reservation(int customerId, int doctorId, String customerName, String doctorName) {
        this.customerId = customerId;
        this.doctorId = doctorId;
        this.customerName = customerName;
        this.doctorName = doctorName;
    }

    public Reservation(String doctorName,int doctorId, String customerName, int customerId, int slotID, String bookingDate, String status) {
        this.bookingDate = bookingDate;
        this.slotID = slotID;
        this.status = status;
        this.customerId = customerId;
        this.doctorId = doctorId;
        this.customerName = customerName;
        this.doctorName = doctorName;
    }

    public Reservation(int doctorId, String customerName, int customerId, int slotID, String bookingDate,  String status) {
        this.bookingDate = bookingDate;
        this.slotID = slotID;
        this.status = status;
        this.customerId = customerId;
        this.doctorId = doctorId;
        this.customerName = customerName;
    }
    
    public Reservation(int reservationID, String doctorName, int doctorId, String customerName, int customerId, String bookingDate, int slotID, String status) {
        this.reservationID = reservationID;
        this.bookingDate = bookingDate;
        this.slotID = slotID;
        this.status = status;
        this.customerId = customerId;
        this.doctorId = doctorId;
        this.customerName = customerName;
        this.doctorName = doctorName;
    }
    

    public int getReservationID() {
        return reservationID;
    }

    public void setReservationID(int reservationID) {
        this.reservationID = reservationID;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    public int getSlotID() {
        return slotID;
    }

    public void setSlotID(int slotID) {
        this.slotID = slotID;
    }

    public Reservation(int reservationID, String bookingDate, int slotID, String status, int customerId) {
        this.reservationID = reservationID;
        this.bookingDate = bookingDate;
        this.slotID = slotID;
        this.status = status;
        this.customerId = customerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getVaccineId() {
        return vaccineId;
    }

    public void setVaccineId(int vaccineId) {
        this.vaccineId = vaccineId;
    }

    public String getVaccineImage() {
        return vaccineImage;
    }

    public void setVaccineImage(String vaccineImage) {
        this.vaccineImage = vaccineImage;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public String getSlotBegin() {
        return slotBegin;
    }

    public void setSlotBegin(String slotBegin) {
        this.slotBegin = slotBegin;
    }

    public String getSlotEnd() {
        return slotEnd;
    }

    public void setSlotEnd(String slotEnd) {
        this.slotEnd = slotEnd;
    }

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    
    @Override
    public String toString() {
        return "Reservation{" + "reservationID=" + reservationID + ", bookingDate=" + bookingDate + ", slotID=" + slotID + ", status=" + status + ", customerId=" + customerId + ", vaccineId=" + vaccineId + ", doctorId=" + doctorId + ", detail=" + detail + ", customerName=" + customerName + ", doctorName=" + doctorName + '}';
    }

    
    
}
