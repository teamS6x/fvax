/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.manager;

import dal.DaoManager;
import dal.DaoReservation;
import dal.DaoVaccine;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Doctor;
import model.Reservation;

/**
 *
 * @author admin
 */
@WebServlet(name = "ReScheduleController", urlPatterns = {"/ReScheduleController"})
public class ReScheduleController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DaoManager dao = new DaoManager();
        String reserID = request.getParameter("reserID");
        Reservation reser = dao.getReservationById(Integer.parseInt(reserID));
        List<Doctor> listDoc = dao.getAllDoctor();
        request.setAttribute("reser", reser);
        request.setAttribute("listDoc", listDoc);
        request.getRequestDispatcher("manager/reSchedule.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String reserId = request.getParameter("reserId");
        String bookingdate = request.getParameter("bookingdate");
//        String bookingdateold = request.getParameter("bookingdateold");
//        try {
//            java.util.Date utilDatenew = new SimpleDateFormat("dd-MMM-yyyy").parse(bookingdate);
//            java.util.Date utilDateold = new SimpleDateFormat("dd-MMM-yyyy").parse(bookingdateold);
//        } catch (ParseException ex) {
//            Logger.getLogger(ReScheduleController.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        java.sql.Date datenew = java.sql.Date.valueOf(request.getParameter("bookingdate"));
//        java.sql.Date dateold = java.sql.Date.valueOf(request.getParameter("bookingdateold"));
        String cusId = request.getParameter("cusId");
        String status = request.getParameter("status");
        String statusold = request.getParameter("statusold");
        new DaoReservation().reSchedule(Integer.parseInt(reserId), bookingdate, status);
        if (status.equals("Pending") && statusold.equals("Failed")) {
            new DaoVaccine().UpdateQuantitySubtract(new DaoReservation().getVaccineIdByReservationId(Integer.parseInt(reserId)));
        }
        //java.util.Date datenew = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss.S").parse(bookingdate);
        //java.util.Date dateold = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss.S").parse(bookingdateold);

//        new DaoReservation().reScheduleForAnotherReser(Integer.parseInt(cusId), utilDatenew, utilDateold);
        response.sendRedirect("schedule");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
