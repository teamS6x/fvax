/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.manager;

import dal.DaoManager;
import dal.DaoVaccine;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Doctor;
import model.Reservation;

/**
 *
 * @author admin
 */
@WebServlet(name = "EditReserStatusController", urlPatterns = {"/editStatusReser"})
public class EditReserStatusController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DaoManager dao = new DaoManager();
        String reserId = request.getParameter("ReservationID");
        Reservation reser = dao.getReservationById(Integer.parseInt(reserId));
        List<Doctor> listDoc = dao.getAllDoctor();
        request.setAttribute("reser", reser);
        request.setAttribute("listDoc", listDoc);
        request.getRequestDispatcher("manager/editReservationStatus.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DaoManager dao = new DaoManager();
        String reserId = request.getParameter("reserId");
        String status = request.getParameter("status");
        dao.updateStatusInReservation(status, Integer.parseInt(reserId));
        int vaccineId = dao.getVaccineIdByReservationId(Integer.parseInt(reserId));
        String statusold = request.getParameter("statusold");
        if (statusold.equals("Pending") && status.equals("Failed")) {
            new DaoVaccine().UpdateQuantity(vaccineId);
        }
        response.sendRedirect("changeStatusReservation");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
