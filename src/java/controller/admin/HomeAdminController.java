/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.admin;

import dal.DaoCustomer;
import dal.DaoDoctor;
import dal.DaoReservation;
import dal.DaoVaccine;
import dal.DaoVaccinePackage;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.ListPackage;
import model.Vaccine;

/**
 *
 * @author a
 */
@WebServlet(name = "ControllerHomeAdmin", urlPatterns = {"/ControllerHomeAdmin"})
public class HomeAdminController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            request.getRequestDispatcher("admin/homeAdmin.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        DaoDoctor daoDoctor = new DaoDoctor();
        DaoCustomer daoCustomer = new DaoCustomer();
        DaoReservation daoReservation = new DaoReservation();
        DaoVaccine daoVaccine = new DaoVaccine();
        DaoVaccinePackage daoVaccinePackage = new DaoVaccinePackage();
        
        int thisYearIncrement = 0, i;
        String thisYearData, lastYearData;
        int numberOfActiveDoctor = daoDoctor.getNumberOfActiveDoctor();
        int numberOfCustomer = daoCustomer.getNumberOfActiveCustomer();
        int numberOfActiveManager = daoDoctor.getNumberOfActiveManager();
        int numberOfSuccessfulReservation = daoReservation.getNumberOfSuccessfulReservation();
        Vaccine v = daoVaccine.getBestSellingVaccine();
        ListPackage lp = daoVaccinePackage.getBestSellingPackage();
        
        HashMap<Integer, String> mapOfBookingNumberCurrentYear = daoReservation
                .getNumberOfBookingsPerMonth(thisYearIncrement);
        HashMap<Integer, String> mapOfBookingNumberLastYear = daoReservation
                .getNumberOfBookingsPerMonth(thisYearIncrement-1);
        
        HashMap<Integer, String> mapOfProfitCurrentYear = daoReservation
                .getMonthlyProfit(thisYearIncrement);
        HashMap<Integer, String> mapOfProfitLastYear = daoReservation
                .getMonthlyProfit(thisYearIncrement-1);
        
        
        String numberOfBookingPerMonthCurrentYearData = "";
        String numberOfBookingPerMonthLastYearData= "";
        String monthlyProfitCurrentYearData = "";
        String monthlyProfitLastYearData = "";
        
        for(i=1; i<13; i++){
            
            if(mapOfBookingNumberCurrentYear.containsKey(i)){
                thisYearData = mapOfBookingNumberCurrentYear.get(i);
                numberOfBookingPerMonthCurrentYearData += thisYearData +",";
            }else{
                numberOfBookingPerMonthCurrentYearData += "0" +",";
            }
            
            if(mapOfBookingNumberLastYear.containsKey(i)){
                lastYearData = mapOfBookingNumberLastYear.get(i);
                numberOfBookingPerMonthLastYearData += lastYearData +",";
            }else{
                numberOfBookingPerMonthLastYearData += "0"+",";
            }
            
            if(mapOfProfitCurrentYear.containsKey(i)){
                thisYearData = mapOfProfitCurrentYear.get(i);
                monthlyProfitCurrentYearData += thisYearData +",";
            }else{
                monthlyProfitCurrentYearData += "0" +",";
            }
            
            if(mapOfProfitLastYear.containsKey(i)){
                lastYearData = mapOfProfitLastYear.get(i);
                monthlyProfitLastYearData += lastYearData +",";
            }else{
                monthlyProfitLastYearData += "0" +",";
            }
        }
        
        
        request.setAttribute("thisYearNumberOfBookingDataset", numberOfBookingPerMonthCurrentYearData);
        request.setAttribute("lastYearNumberOfBookingDataset", numberOfBookingPerMonthLastYearData);
        request.setAttribute("thisYearProfitDataset", monthlyProfitCurrentYearData);
        request.setAttribute("lastYearProfitDataset", monthlyProfitLastYearData);
        request.setAttribute("bestSellingVaccine", v);
        request.setAttribute("bestSellingPackage", lp);
        request.setAttribute("numberOfDoctor", numberOfActiveDoctor);
        request.setAttribute("numberOfManager", numberOfActiveManager);
        request.setAttribute("numberOfCustomer", numberOfCustomer);
        request.setAttribute("numberOfReservation", numberOfSuccessfulReservation);
        request.getRequestDispatcher("admin/homeAdmin.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
